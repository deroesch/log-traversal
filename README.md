# README

Computes MD5 signatures on files and directories, resursively, looking for duplicates.  This was written to clean up a bunch of redundant folders on DropBox.
