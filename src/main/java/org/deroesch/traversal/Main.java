package org.deroesch.traversal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

public class Main {

	private static Logger logger = LogManager.getLogger(Main.class);

	/**
	 * This is the directory used to build the index.
	 */
	static String indexDir = "C:\\Users\\dougl\\Dropbox\\All Unfiled";

	/**
	 * This is the read-only directory we wish to check against for duplicates.
	 */
	static String sourceDir = "C:\\Users\\dougl\\Dropbox\\Sue Gnora";

	/**
	 * This is the index itself.
	 */
	static Map<String, String> index = new HashMap<>();

	/**
	 * main
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// Print the start time
		logger.always().log("---");
		long start = System.currentTimeMillis();
		logger.info(String.format("--- Building index for directory '%s'", indexDir));
		logger.always().log(String.format("--- Started on %s", new Date(start)));
		logger.always().log("---");

		// Build the index
		buildIndex(indexDir, index);

		// Print the end time
		long end = System.currentTimeMillis();
		logger.always().log("---");
		logger.info(String.format("--- Finished with index directory '%s'", indexDir));
		logger.always().log(String.format("--- Finished on %s", new Date(end)));

		// Print the elapsed time
		logger.always().log(String.format("--- Finished in %d milliseconds.", end - start));
		logger.always().log("---");

		// Perform deletes
		deleteDuplicates(sourceDir, index);
		logger.always().log("Done");
	}

	/**
	 * Constructs the reference index
	 * 
	 * @param rootDir the directory used to build the index
	 * @param index   the index itself
	 */
	private static void buildIndex(String rootDir, Map<String, String> index) {
		logger.info(
				String.format("--- File Key --------------------------------------------------- -> --- File Path ---"));
		try {
			// using Files.walkFileTree method in Java 7
			Files.walkFileTree(Paths.get(rootDir), new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult visitFile(Path filePath, BasicFileAttributes attributes) {

					String text = null;
					String path = filePath.toString();

					try {
						byte[] b = Files.readAllBytes(Paths.get(path));
						byte[] h = MessageDigest.getInstance("SHA-256").digest(b);
						text = DatatypeConverter.printHexBinary(h);
					} catch (NoSuchAlgorithmException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}

					logger.info(String.format("%s -> %s", text, path));
					index.put(text, path);
					return FileVisitResult.CONTINUE;
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Constructs the reference index
	 * 
	 * @param rootDir the directory used to build the index
	 * @param index   the index itself
	 */
	private static void deleteDuplicates(String rootDir, Map<String, String> index) {
		try {
			// using Files.walkFileTree method in Java 7
			Files.walkFileTree(Paths.get(rootDir), new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult visitFile(Path filePath, BasicFileAttributes attributes) {

					String text = null;
					String path = filePath.toString();

					try {
						byte[] b = Files.readAllBytes(Paths.get(path));
						byte[] h = MessageDigest.getInstance("SHA-256").digest(b);
						text = DatatypeConverter.printHexBinary(h);
					} catch (NoSuchAlgorithmException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}

					String deletePath = index.get(text);
					if (deletePath != null) {
						try {
							Files.delete(Path.of(deletePath));
							logger.always().log("Deleted duplicate " + deletePath);
						} catch (IOException e) {
							// Ignore
						}
					}

					return FileVisitResult.CONTINUE;
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
